# Notes For RancherOS

# Setup Rancher os
[![Video ](https://img.youtube.com/vi/oMYdErjJDqw/0.jpg)](https://youtu.be/oMYdErjJDqw)

# Setup Rancher on Rancheros
[![Video ](https://img.youtube.com/vi/qL1apIVxgqA/0.jpg)](https://youtu.be/qL1apIVxgqA)
### Config merge
```bash
sudo ros config merge -i file.yml
```
### Install os
```bash
sudo ros install -c file.yml -d /dev/sda --append rancher.password=pass
```
### Change console
```bash
sudo ros console switch ubuntu
```

